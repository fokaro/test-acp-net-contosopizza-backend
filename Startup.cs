using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Host;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.Swagger;

namespace ContosoPizza
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
       // public void ConfigureServices(IServiceCollection services)
       // {

       //     services.AddControllers();
       //     services.AddSwaggerGen(c =>
       //     {
       //         c.SwaggerDoc("v1", new OpenApiInfo { Title = "ContosoPizza", Version = "v1" });
        //    });
      //  }

         public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
 
            services.AddSwaggerGen(c =>
          {
              c.SwaggerDoc("v1", new Info { Title = "SwaggerDemo", Version = "v1" });
           });
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
            c.SwaggerEndpoint("/swagger/v1/swagger.json", "SwaggerDemo v1");
            });
 
            app.UseMvc();
           
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        // public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        // {
        //     app.UseSwagger();

        //     app.UseSwaggerUI(c =>
        //     {
        //     c.SwaggerEndpoint("/swagger/v1/swagger.json", "SwaggerDemo v1");
        //     });
            
        //     if (env.IsDevelopment())
        //     {
        //         app.UseDeveloperExceptionPage();
        //         app.UseSwagger();
        //         app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "ContosoPizza v1"));
        //     }

        //     app.UseRouting();

        //     app.UseAuthorization();

        //     app.UseEndpoints(endpoints =>
        //     {
        //         endpoints.MapControllers();
        //     });
        // }
    }
}
